/*
 * Function for checking, if the medical vehicle has been deployed.
 * Enables the medica facility functionality if deployed
 * Author: Flusinerd (Jan)
 * params: [vehicle]
*/

_vehicle = _this param[0];
if (isNil "_vehicle") exitWith { hint "Err: Vehicle not found"; sleep 5; hintSilent ""};
_isMedicalFacility = false;
// Interval for polling if deployed (in seconds);
_checkInterval = 2;

_isDeployed = false;
_isDeployedLastCheck = false;

while {alive _vehicle} do{
	_tfRange = _vehicle getVariable ["tf_range", 0];

	// Check if vehicle is deployed
	if (_tfRange == 50000) then {
		_isDeployed = true;
	} else {
		_isDeployed = false;
	};

	if !(_isDeployed isEqualTo _isDeployedLastCheck) then {
		// Vehicle deployment status changed
		_vehicle setVariable ["ACE_medical_isMedicalFacility", _isDeployed];
	};
	_isDeployedLastCheck = _isDeployed;
	sleep _checkInterval;
}